package listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

public class ListenersTest implements ITestListener, ISuiteListener, IInvokedMethodListener
{

	@Override
	public void afterInvocation(IInvokedMethod arg0, ITestResult arg1) {
		// TODO Auto-generated method stub
		
		System.out.println("Executed  test method : "+arg0.getTestMethod()+" Status : "+arg1.getStatus());
		Reporter.log("Executed  test method : "+arg0.getTestMethod()+" Status : "+arg1.getStatus(), true);
	}

	@Override
	public void beforeInvocation(IInvokedMethod arg0, ITestResult arg1) {
		// TODO Auto-generated method stub
		Reporter.log("Execution starts on test method : "+arg0.getTestMethod()+" in test Class "+arg1.getTestClass().getName(), true);
	}

	@Override
	public void onFinish(ISuite arg0) {
		// TODO Auto-generated method stub
		Reporter.log("After suite is completed", true);
	}

	@Override
	public void onStart(ISuite arg0) {
		// TODO Auto-generated method stub
		Reporter.log("Before suite is starting", true);
	}

	@Override
	public void onFinish(ITestContext arg0) {
		// TODO Auto-generated method stub
		Reporter.log("Class is completed execution "+arg0.getClass().getName(), true);		
	}

	@Override
	public void onStart(ITestContext arg0) {
		// TODO Auto-generated method stub
		Reporter.log("Class "+arg0.getClass().getName()+" is starting execution", true);
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailure(ITestResult arg0) {
		// TODO Auto-generated method stub
		
		Reporter.log(arg0.getTestName()+" got failed, status "+arg0.getStatus());
	}

	@Override
	public void onTestSkipped(ITestResult arg0) {
		// TODO Auto-generated method stub
		Reporter.log(arg0.getTestName()+" got skipped, status "+arg0.getStatus(), true);
	}

	@Override
	public void onTestStart(ITestResult arg0) {
		// TODO Auto-generated method stub
		Reporter.log(arg0.getTestName()+" started execution, status "+arg0.getStatus(), true);
	}

	@Override
	public void onTestSuccess(ITestResult arg0) {
		// TODO Auto-generated method stub
		Reporter.log(arg0.getTestName()+" test method got executed, status "+arg0.getStatus(), true);
	}
	 
}
