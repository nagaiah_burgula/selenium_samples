package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ActionsTest {

	WebDriver d;
	
	public void initDriver(){
		d = new FirefoxDriver();
	}
	public void launchURL(){
		d.get("http://gmail.com/");
	}
	public static void main(String args[]){
		
		ActionsTest test = new ActionsTest();
		test.initDriver();
		test.launchURL();
	}
}
