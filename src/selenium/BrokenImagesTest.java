package selenium;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class BrokenImagesTest {

	private WebDriver d;
	private int invalidImageCount;
  @BeforeClass
  public void initDriver(){
	  
	  System.setProperty("webdriver.gecko.driver", "D:\\geckodriver-v0.15.0-win64\\geckodriver.exe");
	  d = new FirefoxDriver();
	  d.get("http://www.google.co.in/");
	  d.manage().window().maximize();
  }
  @Test
  public void brokenImagesTest() throws Exception{
	  invalidImageCount = 0;
	  List<WebElement> imagesList = d.findElements(By.tagName("img"));
	  System.out.println("total no of images are : "+imagesList.size());
	  for(WebElement imgEle : imagesList){
		  if(imgEle != null){
			  verifyImageActive(imgEle);
		  }
	  }
	  
  }
  @AfterClass
  public void closeBrowser(){
	  
	  if(d != null)
		  d.quit();
  }
  public void verifyImageActive(WebElement ele) throws Exception{
	  
	  HttpClient client = HttpClientBuilder.create().build();
	  HttpGet get = new HttpGet(ele.getAttribute("src"));
	  HttpResponse res = client.execute(get);
	  if(res.getStatusLine().getStatusCode() != 200){
		  invalidImageCount++;
	  }
  }
}
