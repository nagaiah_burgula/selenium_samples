package selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class GmailTest {

	private WebDriver d;
	
	@Parameters({"browser"})
	@Test
	public void GmailLLoginTest(String browser){
		
		if(browser.equals("chrome")){
			System.setProperty("webdriver.chrome.driver", "D:\\Selenium_Drivers\\Drivers\\chromedriver\\chromedriver.exe");
			d = new ChromeDriver();
			d.get("http://gmail.com");
			d.manage().window().maximize();
			d.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		}else{
			System.setProperty("webdriver.gecko.driver", "D:\\Selenium_Drivers\\Drivers\\geckodriver\\geckodriver");
			d = new FirefoxDriver();
			d.get("http://gmail.com");
			d.manage().window().maximize();
			d.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		}
		WebElement emailField = d.findElement(By.id("Email"));
		SeleniumActions.setHighlight(d, emailField);
		emailField.sendKeys("nagaiahburugala419@gmail.com");
		WebElement nextBtn = d.findElement(By.id("next"));
		SeleniumActions.setHighlight(d, nextBtn);
		nextBtn.click();
		try{
			Thread.sleep(3000);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		WebElement pwdField = d.findElement(By.id("Passwd"));
		SeleniumActions.setHighlight(d, pwdField);
		pwdField.sendKeys("9550954081");
		WebElement staySignInCheck = d.findElement(By.id("PersistentCookie"));
		SeleniumActions.setHighlight(d, staySignInCheck);
		staySignInCheck.click();
		WebElement signInBtn = d.findElement(By.id("signIn"));
		SeleniumActions.setHighlight(d, signInBtn);
		signInBtn.click();
		WebDriverWait wait = new WebDriverWait(d, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'COMPOSE')]")));
		WebElement composeBtn = d.findElement(By.xpath("//div[contains(text(),'COMPOSE')]"));
		SeleniumActions.setHighlight(d, composeBtn);
	
		/*WebElement settingsDD = d.findElement(By.xpath("//div[contains(@aria-label,'Settings')]"));
		SeleniumActions.setHighlight(d, settingsDD);
		settingsDD.click();
		WebElement settingsSelection = d.findElement(By.xpath("//div[@id='ms']/child::div[text()='Settings']"));
		SeleniumActions.setHighlight(d, settingsSelection);
		settingsSelection.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),'Settings')]")));
		WebElement settingsHeader = d.findElement(By.xpath("//h2[contains(text(),'Settings')]"));
		SeleniumActions.setHighlight(d, settingsHeader);*/
		
		WebElement signOutAccount = d.findElement(By.xpath("//a[contains(@href,'https://accounts.google.com/SignOutOptions')]"));
		SeleniumActions.setHighlight(d, signOutAccount);
		signOutAccount.click();
		WebElement signOutBtn = d.findElement(By.id("gb_71"));
		SeleniumActions.setHighlight(d, signOutBtn);
		signOutBtn.click();
		
	}
	@AfterMethod
	public void AfterMethod(){
		d.navigate().refresh();
		try{
			Thread.sleep(3000);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
}
