package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MouseHoverTest {

	private WebDriver d;
	String mouseHoverObject = "if(document.createEvent){var eveObj = document.createEvent('MouseEvents');eveObj.initEvent('mouseover',true,false);arguments[0].dispatchEvent(eveObj);} else if(document.createEvent){arguments[0].fireEvent('onmouseover');}";
	
	@BeforeClass
	public void InitDriver(){
		d = SeleniumActions.getDriver();
		d.get("https://www.flipkart.com/");
		d.manage().window().maximize();
	}
	@Test
	public void mouseHoverTest(){
		
		WebElement electronicsEle = d.findElement(By.xpath("//a[contains(.,'Electronics')][@title='Electronics']"));
		mouseHoverJavascript(electronicsEle);
		/*try{
			Thread.sleep(2000);
		}catch(Exception e){}*/
		WebElement samsuntEle = d.findElement(By.xpath("//a[contains(.,'Samsung')][@title='Samsung'][contains(@href,'/mobiles/samsung')]"));
		WebDriverWait wait = new WebDriverWait(d, 20);
		wait.until(ExpectedConditions.elementToBeClickable(samsuntEle));
		samsuntEle.click();
		System.out.println(d.getCurrentUrl());
	}
	
	public void mouseHoverJavascript(WebElement hoverElement){
		
		try{
		if(isElementPresent(hoverElement)){
	//		String mouseHoverObject = "if(document.createEvent){var eveObj = document.createEvent('MouseEvents');eveObj.initEvent('mouseover',true,false);arguments[0].dispatchEvent(eveObj);} else if(document.createEvent){arguments[0].fireEvent('onmouseover');}";
			
			((JavascriptExecutor)d).executeScript(mouseHoverObject, hoverElement);
		}else{
			
			System.out.println("element was not visible to hover"+"\n");
		}
		}catch(StaleElementReferenceException e){
			System.out.println("Element with "+hoverElement+" is not attached to the page document"+e.getStackTrace());
		}catch(NoSuchElementException e){
			System.out.println("Element with "+hoverElement+" is not found on the DOM"+e.getStackTrace());
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Error occured while hovering "+e.getStackTrace());
		}
	}
	public boolean isElementPresent(WebElement ele){
		
		boolean flag = false;
		try{
			if(ele.isDisplayed() || ele.isEnabled()){
				flag = true;
			}
		}catch(NoSuchElementException e){
			flag = false;
		}catch(StaleElementReferenceException e){
			flag = false;
		}
		return flag;
	}
}
