package selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParameterizedTest {

	private WebDriver d ;
	private String userName = null;
	private String password = null;
	
	@Parameters({"browser","username","password"})
	@BeforeMethod
	public void InitDriver(String browser, String uName, String pwd){

		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
		d = new ChromeDriver();
		userName = uName;
		password = pwd;
		d.get("https://www.facebook.com/login/");
		d.manage().window().maximize();
		d.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}
	@Test
	public void TestMethodOne(){
		
		WebElement emailField = d.findElement(By.id("email"));
		SeleniumActions.setHighlight(d, emailField);
		emailField.sendKeys(userName);
		WebElement pwdField = d.findElement(By.id("pass"));
		SeleniumActions.setHighlight(d, pwdField);
		pwdField.sendKeys(password);
		Reporter.log(userName, true);
		Reporter.log(password, true);
		WebElement logInbtn = d.findElement(By.xpath("//button[contains(text(),'Log In')]"));
		SeleniumActions.setHighlight(d, logInbtn);
		logInbtn.click();
		WebDriverWait wait = new WebDriverWait(d, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='u_0_5'][contains(text(),'Home')]")));
		WebElement accountSettingsDD = d.findElement(By.xpath("//div[contains(text(),'Account Settings')]"));
		SeleniumActions.setHighlight(d, accountSettingsDD);
		accountSettingsDD.click();
		WebElement settingsLink = d.findElement(By.xpath("//a[contains(.,'Settings')][@href='https://www.facebook.com/settings']"));
		SeleniumActions.setHighlight(d, settingsLink);
		settingsLink.click();
		String name= d.findElement(By.xpath("//h3[contains(text(),'Name')]/following-sibling::span/strong")).getText();
		Reporter.log("Name : "+name, true);
		SeleniumActions.setHighlight(d, accountSettingsDD);
		accountSettingsDD.click();
		WebElement logoutLink = d.findElement(By.xpath("//span[contains(text(),'Log out')]"));
		SeleniumActions.setHighlight(d, logoutLink);
		logoutLink.click();
		
		
	}
	@AfterMethod
	public void CloseDriver(){
		d.close();
	}
	
}
