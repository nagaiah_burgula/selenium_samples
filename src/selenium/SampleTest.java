package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SampleTest {

  private WebDriver d;
  
  @BeforeMethod
  public void initDriver(){
	d =  SeleniumActions.getDriver();
	d.get("http://content.icicidirect.com");
  }
  @Test
  public void sampleTestMethod() {
	 
	  WebElement accountDD = d.findElement(By.id("open_account"));
	  accountDD.click();
//	  WebElement corporateAcc = d.findElement(By.xpath("//option[contains(text(),'Corporate Account')]"));
	  Select selectOption = new Select(accountDD);
	  selectOption.selectByVisibleText("Corporate Account");
	  WebElement sele = selectOption.getFirstSelectedOption();
	  Reporter.log(sele.getText(), true);
	  Reporter.log(d.getCurrentUrl(), true);
	  String selectedOption = accountDD.getText(); 
	  WebElement myAccountBtn = d.findElement(By.xpath("//input[contains(@value,'Open my Account')]"));
	  myAccountBtn.click();
	  try{
		  Thread.sleep(3000);
	  }catch(Exception e){
		  e.getMessage();
	  }
	  
	  Reporter.log(d.getCurrentUrl(), true);
//    Reporter.log(selectedOption, true);
	  
  }
  @AfterMethod
  public void CloseDriver(){
	  
	  d.close();
  }
}
