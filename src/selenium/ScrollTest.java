package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ScrollTest {

	private WebDriver d;
	String url = "http://seleniummaster.com";
	@BeforeClass
	public void beforeClass(){
		
		
		d = SeleniumActions.getDriver();
		d.get(url);
		d.manage().window().maximize();
		System.out.println("executing Before class ");
	}
	@Test
	public void testMethodOne(){
		
	WebElement ele1 = d.findElement(By.xpath("//a[@title='End' and contains(text(),'End')]"));
	JavascriptExecutor js = (JavascriptExecutor)d;
	js.executeScript("document.scrollIntoView(true);", ele1);
		System.out.println("in test method ");
	}
	@AfterClass
	public void afterClass(){
		
		System.out.println("executing after class ");
	}
}
