package selenium;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumActions {

	public static void setHighlight(WebDriver d, WebElement ele){
		
		JavascriptExecutor js = ((JavascriptExecutor)d);
		js.executeScript("arguments[0].setAttribute('style','border: solid 3px blue')", ele);
	}
	public static WebDriver getDriver()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium_Drivers\\Drivers\\chromedriver\\chromedriver.exe");
		WebDriver d = new ChromeDriver();
		d.manage().window().maximize();
		return d;
	}
}
